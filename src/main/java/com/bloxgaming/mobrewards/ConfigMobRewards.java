package com.bloxgaming.mobrewards;

import com.forgeessentials.core.moduleLauncher.config.ConfigLoaderBase;
import com.forgeessentials.util.output.LoggingHandler;
import net.minecraftforge.common.config.Configuration;

/**
 * Created by Gregory Maddra on 2016-05-30.
 */
public class ConfigMobRewards extends ConfigLoaderBase {
    @Override
    public void load(Configuration configuration, boolean b) {
        LoggingHandler.felog.debug("Loading MobRewards config");
        BloxMobRewardsModule.multiplier = configuration.get("Reward Balancing", "Reward Multiplier", 1, "Multiply reward payouts by this value. 1 is about 1/10th of mob health.").getInt();
    }
}
