package com.bloxgaming.mobrewards;

import com.forgeessentials.api.APIRegistry;
import com.forgeessentials.api.UserIdent;
import com.forgeessentials.api.economy.Wallet;
import com.forgeessentials.core.ForgeEssentials;
import com.forgeessentials.core.moduleLauncher.FEModule;
import com.forgeessentials.util.events.FEModuleEvent;
import com.forgeessentials.util.events.ServerEventHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.permission.PermissionLevel;
import net.minecraftforge.permission.PermissionManager;

/**
 * Created by Gregory Maddra on 2016-04-30.
 */
@FEModule(name = "Mob Rewards", parentMod = ForgeEssentials.class, version = "2.0")
public class BloxMobRewardsModule extends ServerEventHandler{

    public static int multiplier = 1;

    @SubscribeEvent
    public void load(FEModuleEvent.FEModuleInitEvent event){
        PermissionManager.registerPermission("bloxgaming.mr.reward", PermissionLevel.TRUE);
        ForgeEssentials.getConfigManager().registerLoader("MobRewards", new ConfigMobRewards());
    }

    @SubscribeEvent
    public void serverStarting(FEModuleEvent.FEModuleServerInitEvent event){
        APIRegistry.perms.registerPermission("bloxgaming.mr.reward", PermissionLevel.TRUE, "Give rewards for killing mobs");
        APIRegistry.perms.registerPermission("bloxgaming.mr.reward.passive", PermissionLevel.FALSE, "Reward for killing hostile mobs");
    }

    @SubscribeEvent
    public void onMobDeath(LivingDeathEvent event){
        if(event.source.getEntity() instanceof EntityPlayerMP && event.entity instanceof EntityLiving){
            EntityPlayerMP ply = (EntityPlayerMP)event.source.getEntity();
            int money = (int)event.entityLiving.getMaxHealth()/10;
            //They at least need to get some money for killing things.
            if (money == 0){
                money = 1;
            }
            UserIdent plyIdent = UserIdent.get(ply);
            if(!plyIdent.isFakePlayer() && plyIdent.checkPermission("bloxgaming.mr.reward") && (plyIdent.checkPermission("bloxgaming.mr.reward.passive") || event.entity instanceof IMob)) {
                Wallet wallet = APIRegistry.economy.getWallet(plyIdent);
                money *= multiplier;
                wallet.add(money);
                String message = "You have been paid " + EnumChatFormatting.GREEN + APIRegistry.economy.toString(money) + EnumChatFormatting.RESET + " for killing a " + event.entityLiving.getCommandSenderName();
                ply.addChatMessage(new ChatComponentText(message));
            }
        }
    }
}
